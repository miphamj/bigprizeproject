import React, { Component } from 'react';
import moment from 'moment';
import { Col, Row } from 'antd';

import './banner-header.scss';

export class BannerHeader extends Component {

  constructor(props) {
    super(props);
    const { time } = props.imgSource;
    const expriedTime = moment(time, 'DD-MM-YYYY').startOf('day').unix();
    const nowTime = moment().unix();
    this.state = {
      distance: expriedTime - nowTime,
    }
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      if (this.state.distance > 0) {
        this.setState({ distance: this.state.distance - 1 })
      } else {
        clearInterval(this.interval)
      }
    }, 1000)
  }

  getTimeShow() {
    const { distance } = this.state;
    return {
      days: Math.floor(distance / (60 * 60 * 24)),
      hours: Math.floor((distance % (60 * 60 * 24)) / (60 * 60)),
      minutes: Math.floor((distance % (60 * 60)) / 60),
      seconds: Math.floor(distance % 60)
    }
  }

  render() {
    const { imgSource } = this.props;

    const time = this.getTimeShow();
    return (
      <div className="banner-header">
        <div className="summary">
          <h2 className="summary-title">{imgSource.name}</h2>
          <Row className="summary-info ">
            <div className="mobile-scr">
              <Col xs={24} md={8} className="info-item">
                <img src={require('../images/location.png')} /> {imgSource.add}
              </Col>
              <Col xs={24} md={8} className="info-item">
                <img src={require('../images/time.png')} /> {imgSource.time}
              </Col>
              <Col xs={24} md={8} className="info-item">
                <img src={require('../images/distance.png')} /> {imgSource.distance}
              </Col>
            </div>
            <div className='desktop-scr'>
              <span><img src={require('../images/location.png')} /> {imgSource.add}</span>
              <span><img src={require('../images/time.png')} /> {imgSource.time}</span>
              <span><img src={require('../images/distance.png')} /> {imgSource.distance}</span>
            </div>
          </Row>
          <div className="countdown">
            <div className="countdown-item">
              <div className="up">{time.days}</div>
              <div className="down">Ngày</div>
            </div>
            <span className="slash"></span>
            <div className="countdown-item">
              <div className="up">{time.hours}</div>
              <div className="down">Giờ</div>
            </div>
            <span className="slash"></span>
            <div className="countdown-item">
              <div className="up">{time.minutes}</div>
              <div className="down">Phút</div>
            </div>
            <span className="slash"></span>
            <div className="countdown-item">
              <div className="up">{time.seconds}</div>
              <div className="down">Giây</div>
            </div>
          </div>
          <div className="btn-more">
            <div><button>Đăng ký ngay</button></div>
          </div>
        </div>
        <img className="img-header" src={imgSource.banner} />
      </div>
    )
  }
}