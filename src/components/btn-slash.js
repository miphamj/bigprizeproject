import React, { Component } from 'react';

import './btn-slash.scss';

export class BtnSlash extends Component {

  constructor(props) {
    super(props);
  }

  render() {
      const {name} = this.props;
    return (
      <div className="btn-slash">
        <div><button>{name}</button></div>
      </div>
    )
  }
}