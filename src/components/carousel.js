import React, { Component } from 'react' 
import { Carousel } from 'antd';

import './carousel.scss';

export class HeaderCarousel extends Component {

  constructor(props){
    super(props);
    
  }
  
  render() {
    const {height, bgImg} = this.props;
    const element = bgImg.map((item,index) => {
        return (
            <div key={index}>
                <img className={height === "500" ? "carousel-img-500" : "carousel-img"} src={item} />
            </div>
        )
    })
    return (
      <div className="carousel">
        <Carousel autoplay>
          {element}
        </Carousel>
      </div>
    )
  }
}