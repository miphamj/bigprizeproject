import React, { Component } from 'react';

import './category-title.scss';

export class CategoryTitle extends Component {

  constructor(props){
    super(props);
  }
  
  render() {
    const { name } = this.props;
    return (
      <div className="category-title">
        <p className="title-name">{name}</p>
        <div className='underline'>
            <img src={require('../images/underline.png')} />
        </div>
      </div>
    )
  }
}