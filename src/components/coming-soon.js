import React, { Component } from 'react';
import { Button } from 'antd';
import ItemsCarousel from 'react-items-carousel';

import './coming-soon.scss';
import { CategoryTitle } from './category-title';
import { Tournament } from './tournament';

export class ComingSoon extends Component {

    constructor(props) {
        super(props);
        this.data = [
            {
                id: 1,
                name: 'TAY HO HALF MARATHON 2020', 
                description: 'Nhiều cự ly chạy, đa dạng. phù hợp với cả những VĐV đã tập chạy lâu năm và cả với các bạn mới tập chạy, các trẻ nhỏ đối tượng', 
                add: 'Sân bóng An Dương, Tây Hồ, Hà Nội', 
                time: '16/02/2020 04:30', distance: '3 km | 6 km | 15 km | 21 km', 
                src: require('../images/bg09.jpg'), 
                banner: require('../images/bg08.jpg'),
                expriedDate: '15/01/2020',
                tournamentInfo: {
                    title: 'TAY HO HALF MARATHON 2020 là cơ hội tuyệt vời để những người yêu thể thao bắt đầu một năm mới đong đầy sức khỏe, ngập tràn niềm vui.',
                    description: `<p>Cung đường chạy quanh Hồ Tây vô cùng thoáng đãng và đặc sắc. Đường chạy đi qua rất nhiều không gian văn hoá của thủ đô Hà Nội, nơi mà thời 
                    gian như lắng đọng: đền Quán Thánh, chùa Trấn Quốc, phủ Tây Hồ và vô số danh thắng bậc nhất Hà Thành. Các VĐV hứa hẹn sẽ được tham gia một mùa du xuân chẩy 
                    hội theo cách độc đáo chưa từng có. </p>
                    <p>Đây là giải đầu tiên, và duy nhất được UBND Quận Tây Hồ tổ chức, do vậy Quận sẽ thực hiện cấm đường để đảm bảo các VĐV tham gia giải chạy và du xuân độc 
                    đáo một cách an toàn nhất. </p>
                    <p>Bên cạnh đó, Tết sẽ thêm vui khi ta làm điều ý nghĩa. Mỗi vận động viên tham gia Giải đồng nghĩa với đóng góp 50,000 VND vào Quỹ Trẻ Em quận Tây Hồ. Hãy 
                    đến với TAY HO HALF MARATHON 2020 và cùng lan tòa niềm vui ngày Tết đi muôn nơi.</p>`
                },
                registerInfo: [
                    {
                        key: '1',
                        distance: '21km',
                        earlyBird: '650.000',
                        standard: '799.000',
                        late: '1.250.000'
                    },
                    {
                        key: '2',
                        distance: '15km',
                        earlyBird: '550.000',
                        standard: '699.000',
                        late: '1.150.000'
                    },
                    {
                        key: '3',
                        distance: '6km',
                        earlyBird: '450.000',
                        standard: '599.000',
                        late: '900.000'
                    },
                    {
                        key: '4',
                        distance: '3km',
                        earlyBird: '350.000',
                        standard: '499.000',
                        late: '700.000'
                    },
                ],
                registerExpiredDate: {
                    EPMEarlyBird: 'until 15/12',
                    EPMStandard: 'until 12/01',
                    EPMLate: '12/01-03/02'
                },
                 registerRule: `<span style="font-weight: 700">*</span> Family Ticket: 01 bố + 01 mẹ + 01 trẻ dưới 12 tuổi, giảm 10% tổng hóa đơn<br/>
                 <span style="font-weight: 700">*</span> Group Discount: `,
                 groupDiscount: [
                    {
                        key: '1',
                        group: '10 people',
                        discount: '5%',
                    },
                    {
                        key: '2',
                        group: '30 people',
                        discount: '8%',
                    },
                    {
                        key: '3',
                        group: '60 people',
                        discount: '12%',
                    },
                    {
                        key: '4',
                        group: '100  people and more',
                        discount: '17%',
                    },
                ]
                
            },
            { 
                id: 2, name: 'Template Marathon 2020', 
                description: 'Nhiều cự ly chạy, đa dạng. phù hợp với cả những VĐV đã tập chạy lâu năm và cả với các bạn mới tập chạy, các trẻ nhỏ đối tượng', 
                add: 'Hà Nội', 
                time: '16/02/2020 04:30', distance: '3 km | 6 km | 15 km | 21 km', 
                src: require('../images/template.png'), 
                banner: require('../images/template.png'),
                expriedDate: '20/05/2020',
                tournamentInfo: {
                    title: 'TAY HO HALF MARATHON 2020 là cơ hội tuyệt vời để những người yêu thể thao bắt đầu một năm mới đong đầy sức khỏe, ngập tràn niềm vui.',
                    description: `<p>Cung đường chạy quanh Hồ Tây vô cùng thoáng đãng và đặc sắc. Đường chạy đi qua rất nhiều không gian văn hoá của thủ đô Hà Nội, nơi mà thời 
                    gian như lắng đọng: đền Quán Thánh, chùa Trấn Quốc, phủ Tây Hồ và vô số danh thắng bậc nhất Hà Thành. Các VĐV hứa hẹn sẽ được tham gia một mùa du xuân chẩy 
                    hội theo cách độc đáo chưa từng có. </p>
                    <p>Đây là giải đầu tiên, và duy nhất được UBND Quận Tây Hồ tổ chức, do vậy Quận sẽ thực hiện cấm đường để đảm bảo các VĐV tham gia giải chạy và du xuân độc 
                    đáo một cách an toàn nhất. </p>
                    <p>Bên cạnh đó, Tết sẽ thêm vui khi ta làm điều ý nghĩa. Mỗi vận động viên tham gia Giải đồng nghĩa với đóng góp 50,000 VND vào Quỹ Trẻ Em quận Tây Hồ. Hãy 
                    đến với TAY HO HALF MARATHON 2020 và cùng lan tòa niềm vui ngày Tết đi muôn nơi.</p>`
                },
                registerInfo: [
                    {
                        key: '1',
                        distance: 'FM 42km',
                        priority: 'Sold out',
                        superEarlyBird: 'Sold out',
                        earlyBird: 'Soldout',
                        standard: '999.000',
                        late: '1.290.000'
                    },
                    {
                        key: '2',
                        distance: 'HM 21km',
                        priority: 'Sold out',
                        superEarlyBird: 'Sold out',
                        earlyBird: 'Soldout',
                        standard: '999.000',
                        late: '1.290.000'
                    },
                    {
                        key: '3',
                        distance: '10km',
                        priority: 'Sold out',
                        superEarlyBird: 'Sold out',
                        earlyBird: 'Soldout',
                        standard: '999.000',
                        late: '1.290.000'
                    },
                    {
                        key: '4',
                        distance: '5km',
                        priority: 'Sold out',
                        superEarlyBird: 'Sold out',
                        earlyBird: 'Soldout',
                        standard: '999.000',
                        late: '1.290.000'
                    },
                    {
                        key: '5',
                        distance: '5km Kids run (<12 years)',
                        priority: 'Sold out',
                        superEarlyBird: 'Sold out',
                        earlyBird: 'Soldout',
                        standard: '999.000',
                        late: '1.290.000'
                    },
                ],
                registerExpiredDate: {
                    EPMPriorit: '19.9-21.9',
                    EPMSuperEarlyBird: '22.9-29.9',
                    EPMEarlyBird: '30.9-20.10',
                    EPMStandard: 'until 31.12',
                    EPMLate: '01.01.19-15.01.20'
                },
                 registerRule: `<span style="font-weight: 700">* Family Ticket: 01 bố + 01 mẹ + 01 trẻ dưới 12 tuổi, giảm 10% tổng hóa đơn</span>`
                
            },
            { 
                id: 3, name: 'Template Marathon 3rd Ekiden', 
                description: 'Nhiều cự ly chạy, đa dạng. phù hợp với cả những VĐV đã tập chạy lâu năm và cả với các bạn mới tập chạy, các trẻ nhỏ đối tượng', 
                add: 'Hà Nội', 
                time: '16/02/2020 04:30', distance: '3 km | 6 km | 15 km | 21 km', 
                src: require('../images/template.png'), 
                banner: require('../images/template.png'),
                expriedDate: '20/05/2020',
                tournamentInfo: {
                    title: 'TAY HO HALF MARATHON 2020 là cơ hội tuyệt vời để những người yêu thể thao bắt đầu một năm mới đong đầy sức khỏe, ngập tràn niềm vui.',
                    description: `<p>Cung đường chạy quanh Hồ Tây vô cùng thoáng đãng và đặc sắc. Đường chạy đi qua rất nhiều không gian văn hoá của thủ đô Hà Nội, nơi mà thời 
                    gian như lắng đọng: đền Quán Thánh, chùa Trấn Quốc, phủ Tây Hồ và vô số danh thắng bậc nhất Hà Thành. Các VĐV hứa hẹn sẽ được tham gia một mùa du xuân chẩy 
                    hội theo cách độc đáo chưa từng có. </p>
                    <p>Đây là giải đầu tiên, và duy nhất được UBND Quận Tây Hồ tổ chức, do vậy Quận sẽ thực hiện cấm đường để đảm bảo các VĐV tham gia giải chạy và du xuân độc 
                    đáo một cách an toàn nhất. </p>
                    <p>Bên cạnh đó, Tết sẽ thêm vui khi ta làm điều ý nghĩa. Mỗi vận động viên tham gia Giải đồng nghĩa với đóng góp 50,000 VND vào Quỹ Trẻ Em quận Tây Hồ. Hãy 
                    đến với TAY HO HALF MARATHON 2020 và cùng lan tòa niềm vui ngày Tết đi muôn nơi.</p>`
                },
                registerInfo: [
                    {
                        key: '1',
                        distance: 'FM 42km',
                        priority: 'Sold out',
                        superEarlyBird: 'Sold out',
                        earlyBird: 'Soldout',
                        standard: '999.000',
                        late: '1.290.000'
                    },
                    {
                        key: '2',
                        distance: 'HM 21km',
                        priority: 'Sold out',
                        superEarlyBird: 'Sold out',
                        earlyBird: 'Soldout',
                        standard: '999.000',
                        late: '1.290.000'
                    },
                    {
                        key: '3',
                        distance: '10km',
                        priority: 'Sold out',
                        superEarlyBird: 'Sold out',
                        earlyBird: 'Soldout',
                        standard: '999.000',
                        late: '1.290.000'
                    },
                    {
                        key: '4',
                        distance: '5km',
                        priority: 'Sold out',
                        superEarlyBird: 'Sold out',
                        earlyBird: 'Soldout',
                        standard: '999.000',
                        late: '1.290.000'
                    },
                    {
                        key: '5',
                        distance: '5km Kids run (<12 years)',
                        priority: 'Sold out',
                        superEarlyBird: 'Sold out',
                        earlyBird: 'Soldout',
                        standard: '999.000',
                        late: '1.290.000'
                    },
                ],
                registerExpiredDate: {
                    EPMPriorit: '19.9-21.9',
                    EPMSuperEarlyBird: '22.9-29.9',
                    EPMEarlyBird: '30.9-20.10',
                    EPMStandard: 'until 31.12',
                    EPMLate: '01.01.19-15.01.20'
                },
                 registerRule: `<span style="font-weight: 700">* Family Ticket: 01 bố + 01 mẹ + 01 trẻ dưới 12 tuổi, giảm 10% tổng hóa đơn</span>`
                
            },
        ];
        this.state = {
            activeItemIndex: 0,
        }
    }

    handleTourDetail = (item) => {
        this.props.history.push("/tournament-detail", {item: item});
    }

    render() {
        const { activeItemIndex } = this.state;
        const chevronWidth = 40;
        const noOfCards = window.innerWidth >= 992 ? 3: window.innerWidth >= 768 ? 2 : 1;
        const element = this.data.map(item => {
            return (
                <span key={item.id} onClick={() => this.handleTourDetail(item)}>
                    <Tournament item={item} background="red" />
                </span>
                
            )
        })
        return (
            <div className="coming-soon">
                <CategoryTitle name="Giải đấu sắp diễn ra" />
                <section className="blank-space"></section>
                <div className="coming-background">
                    <div className="coming-content">
                        <ItemsCarousel
                            infiniteLoop
                            gutter={12}
                            numberOfCards={noOfCards}
                            activeItemIndex={activeItemIndex}
                            requestToChangeActive={index => {
                                this.setState({ activeItemIndex: index })
                            }}
                            rightChevron={
                                <Button className="btn-chevron-right">
                                    <img src={require('../images/right-arrow.jpg')} />
                                </Button>
                            }
                            leftChevron={
                                <Button className="btn-chevron-left">
                                    <img src={require('../images/left-arrow.jpg')} />
                                </Button>
                            }
                            outsideChevron
                            chevronWidth={chevronWidth}
                        >
                            {element}
                        </ItemsCarousel>
                    </div>
                    <div className="btn-more">
                        <div><button>Xem tất cả</button></div>
                    </div>
                </div>
                <section className="blank-space"></section>
            </div>
        )
    }
}