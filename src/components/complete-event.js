import React, { Component } from 'react';
import { Button } from 'antd';
import ItemsCarousel from 'react-items-carousel';

import './complete-event.scss';
import { CategoryTitle } from './category-title';
import { Tournament } from './tournament';

export class CompleteEvent extends Component {

    constructor(props) {
        super(props);
        this.data = [
            { id: 1, name: 'HANOI CITY TRAIL RUN 2020', description: 'Một giải chạy địa hình ngay tại thủ đô Hà Nội! Không cần phải đi quá xa, trèo đèo lội suối để... ', add: 'Sân bóng An Dương, Tây Hồ, Hà Nội', time: '01/01/2020 04:30', distance: '5 km | 10 km | 15 km', src: require('../images/finish01.jpg') },
            { id: 2, name: 'Sunset Bay Triathlon 2019', description: 'Một giải chạy địa hình ngay tại thủ đô Hà Nội! Không cần phải đi quá xa, trèo đèo lội suối để... ', add: 'Sân bóng An Dương, Tây Hồ, Hà Nội', time: '01/01/2020 04:30', distance: '5 km | 10 km | 15 km', src: require('../images/finish02.jpg') },
            { id: 3, name: 'Longbien Marathon 3rd Ekiden', description: 'Một giải chạy địa hình ngay tại thủ đô Hà Nội! Không cần phải đi quá xa, trèo đèo lội suối để... ', add: 'Sân bóng An Dương, Tây Hồ, Hà Nội', time: '01/01/2020 04:30', distance: '5 km | 10 km | 15 km', src: require('../images/finish03.jpg') },
        ];
        this.state = {
            activeItemIndex: 0,
        }
    }

    render() {
        const { activeItemIndex } = this.state;
        const chevronWidth = 40;
        const noOfCards = window.innerWidth >= 992 ? 3: window.innerWidth >= 768 ? 2 : 1;
        const element = this.data.map(item => {
            return (
                <Tournament key={item.id} item={item} background="white" />
            )
        })
        return (
            <div className="complete-event">
                <CategoryTitle name="Giải đấu đã hoàn thành" />
                <div className="complete-background">
                    <div className="complete-content">
                    <ItemsCarousel
                            infiniteLoop
                            gutter={12}
                            numberOfCards={noOfCards}
                            activeItemIndex={activeItemIndex}
                            requestToChangeActive={index => {
                                this.setState({ activeItemIndex: index })
                            }}
                            rightChevron={
                                <Button className="btn-chevron-right">
                                    <img src={require('../images/right-arrow.jpg')} />
                                </Button>
                            }
                            leftChevron={
                                <Button className="btn-chevron-left">
                                    <img src={require('../images/left-arrow.jpg')} />
                                </Button>
                            }
                            outsideChevron
                            chevronWidth={chevronWidth}
                        >
                            {element}
                        </ItemsCarousel>
                    </div>
                    <div className="btn-more">
                        <div><button>Xem tất cả</button></div>
                    </div>
                </div>
            </div>
        )
    }
}