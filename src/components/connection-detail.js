import React, { Component } from 'react';
import ItemsCarousel from 'react-items-carousel';
import { Button, Icon } from 'antd';

import './connection-detail.scss';

export class ConnectionDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activeItemIndex: 0,
        }
    }

    render() {
        const { name, item } = this.props;
        const { activeItemIndex } = this.state;
        const chevronWidth = 40;
        const noOfCards = window.innerWidth >= 768 ? 6 : 2;
        const organization = item.map(item => {
            return (
                <a key={item.id} target="_blank" href={item.website}><img className="logo-brand" src={item.src} /></a>
            )
        });
        return (
            <div className="connection-detail">
                <div className="connection-title"><span>{name}</span></div>
                <div className='middle-line'></div>
                <ItemsCarousel
                    infiniteLoop
                    gutter={12}
                    numberOfCards={noOfCards}
                    activeItemIndex={activeItemIndex}
                    requestToChangeActive={index => {
                        this.setState({ activeItemIndex: index })
                    }}
                    rightChevron={
                        <Button className="btn-chevron-right">
                            <img src={require('../images/right-arrow.jpg')} />
                        </Button>
                    }
                    leftChevron={
                        <Button className="btn-chevron-left">
                            <img src={require('../images/left-arrow.jpg')} />
                        </Button>
                    }
                    outsideChevron
                    chevronWidth={chevronWidth}
                >
                    {organization}
                </ItemsCarousel>
            </div>
        )
    }
}