import React, { Component } from 'react';

import './connection.scss';
import { CategoryTitle } from './category-title';
import { ConnectionDetail } from './connection-detail';

export class Connection extends Component {

    constructor(props) {
        super(props);
        this.dataOrganization = [
            { id: 0, src: require('../images/lining.jpg'), website: 'https://lining.com.vn/' },
            { id: 1, src: require('../images/coupletx.jpg'), website: 'https://coupletx.com/' },
            { id: 2, src: require('../images/gamuda.jpg'), website: 'https://gamudaland.com.vn/' },
            { id: 3, src: require('../images/bumgroup.jpg'), website: 'https://bimgroup.com/' },
            { id: 4, src: require('../images/fastgo.jpg'), website: 'https://fastgo.mobi/' },
            { id: 5, src: require('../images/viettel.jpg'), website: 'https://viettel.vn/' },
        ];
        this.dataCustomer = [
            { id: 0, src: require('../images/bidv.jpg'), website: 'https://ebank.bidv.com.vn/' },
            { id: 1, src: require('../images/vietcombank.jpg'), website: 'https://www.vietcombank.com.vn' },
            { id: 2, src: require('../images/gpbank.jpg'), website: 'https://www.gpbank.com.vn/' },
            { id: 3, src: require('../images/vpbank.jpg'), website: 'https://www.vpbank.com.vn/' },
            { id: 4, src: require('../images/techcombank.jpg'), website: 'https://www.techcombank.com.vn/' },
            { id: 5, src: require('../images/tpbank.jpg'), website: 'https://tpb.vn/' },
        ]
        this.state = {
            activeItemIndex: 0,
            setActiveItemIndex: 0
        }
    }

    render() {
        return (
            <div className="connection">
                <section className="blank-space"></section>
                <div className="connection-bg">
                    <CategoryTitle name="Đối tác và khách hàng" />
                    <div className="connection-content">
                        <div className='organization'>
                            <ConnectionDetail name="Đối tác tổ chức" item={this.dataOrganization} />
                        </div>
                        <div className='customer'>
                            <ConnectionDetail name="Khách hàng" item={this.dataCustomer} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}