import React, { Component } from 'react';
import { Row, Col } from 'antd';
import { Link } from 'react-router-dom';

import './footer.scss';

export class Footer extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="footer">
                <div className="footer-bg">
                    <Row>
                        <Col className="col" xs={24} md={7}>
                            <img className="footer-logo" src={require('../images/logo-footer.png')} />
                            <p>Đối với yêu cầu hợp tác, báo chí, gợi ý và phản hồi, vui lòng liên hệ với chúng tôi trực tiếp hoặc gửi email cho chúng tôi tại
                            contact@bigprize.vn</p>
                        </Col>
                        <Col className="col" xs={24} md={7}>
                            <h4>Thông tin liên hệ</h4>
                            <p><span>Địa chỉ: </span>Tầng 6, tòa nhà An Huy, số 184, đường Nguyễn Tuân, Thanh Xuân, Hà Nội.</p>
                            <p><span>Điện thoại: </span>093 442 44 00</p>
                            <p><span>Email: </span>contact@bigprize.vn</p>
                        </Col>
                        <Col className="col" xs={24} md={4}>
                            <h4>Về chúng tôi</h4>
                            <span className="large-scr">
                                <p><Link to ="/introduce" >Giới thiệu</Link></p>
                                <p><Link to ="/services" >Dịch vụ</Link></p>
                                <p><Link to ="/news" >Tin tức</Link></p>
                                <p><Link to ="/contact" >Liên hệ</Link></p>
                            </span>
                            <div className="mobile-scr">
                                <p><Link to ="/introduce" >Giới thiệu</Link>&nbsp;&nbsp;|&nbsp;&nbsp;<Link to ="/services" >Dịch vụ</Link>
                                &nbsp;&nbsp;|&nbsp;&nbsp;<Link to ="/news" >Tin tức</Link>&nbsp;&nbsp;|&nbsp;&nbsp;<Link to ="/contact" >Liên hệ</Link></p>
                            </div>
                        </Col>
                        <Col className="col" xs={24} md={6}>
                            <h4>Liên kết mạng xã hội</h4>
                            <div className="sns">
                                <img src={require('../images/facebook.png')} />
                                <img src={require('../images/twitter.png')} />
                                <img src={require('../images/youtube.png')} />
                            </div>
                            <p>© 2019 BigPrize. All right Reserved</p>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}