import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { Button,Drawer, Icon } from 'antd';

import './header.scss';
import logo from '../images/logo.png';

export class Header extends Component {

  constructor(props) {
    super(props);
    this.menuList = [
      { name: '/home', value: 'Trang chủ' },
      { name: 'slash', value: "/" },
      { name: '/introduce', value: "Giới thiệu" },
      { name: 'slash', value: "/" },
      { name: '/services', value: "Dịch vụ" },
      { name: 'slash', value: "/" },
      { name: '/news', value: "Tin tức" },
      { name: 'slash', value: "/" },
      { name: '/contact', value: "Liên hệ" }
    ]
    this.state = { visible: false };
  }

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    console.log('ahihi');
    this.setState({
      visible: false,
    });
  };

  render() {
    const { pathName } = this.props;
    const element = this.menuList.map((item, index) => {
      return (item.name === 'slash'
        ? <li key={index} className='active slash' key={index}>{item.value}</li>
        : <li key={index} className={item.name === `${pathName}` ? 'active' : ''} key={index}>
          <Link className='menu' to={item.name}>
            {item.value}
          </Link>
        </li>
      )
    })
    return (
      <div className="header">
        <div className="max-width">
            <Link className="logo-img" to="/home"><img src={logo} /></Link>
          <div className="large-screen">
            <ul>
              {element}
            </ul>
          </div>
          <div className="mobile-screen">
            <Button onClick={this.showDrawer}><Icon type="align-right" /></Button>
          </div>
        </div>
        <Drawer
          title={<span><Icon type="close" onClick={this.onClose}/></span>}
          placement="right"
          closable={false}
          onClose={this.onClose}
          visible={this.state.visible}
          getContainer={false}
        >
          <ul className="side-menu">
            <li><Link to="/home" className={'/home' === `${pathName}` ? 'active' : ''}>Trang chủ</Link></li>
            <li><Link to="/introduce" className={'/introduce' === `${pathName}` ? 'active' : ''}>Giới thiệu</Link></li>
            <li><Link to="/services" className={'/services' === `${pathName}` ? 'active' : ''}>Dịch vụ</Link></li>
            <li><Link to="/news" className={'/news' === `${pathName}` ? 'active' : ''}>Tin tức</Link></li>
            <li><Link to="/contact" className={'/contact' === `${pathName}` ? 'active' : ''}>Liên hệ</Link></li>
          </ul>
        </Drawer>
      </div>
    )
  }
}