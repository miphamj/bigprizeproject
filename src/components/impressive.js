import React, { Component } from 'react';
import { CategoryTitle } from './category-title';
import { Row, Col } from 'antd';

import './impressive.scss';

export class Impressive extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { item, background } = this.props;
        return (
            <div className='impressive'>
                <div className="impress-title">
                    <CategoryTitle name="Những con số ấn tượng" />
                </div>
                <img className="impressive-bg" src={require('../images/bgcontent02.png')} />
                <img className="impressive-mobile-bg" src={require('../images/mobile-bg.jpg')} />
                <div className="statistic">
                    <Row className="detail">   
                        <Col xs={24} md={6}>
                            
                        </Col>
                        <Col xs={24} md={6}>
                            <h1>+8000</h1>
                            <h5>Người theo dõi fanpage</h5>
                        </Col>
                        <Col xs={24} md={6}>
                            <h1>+7000</h1>
                            <h5>Người tham gia chạy</h5>
                        </Col>
                        <Col xs={24} md={6}>
                            <h1>+3000</h1>
                            <h5>Người tham gia chạy ảo</h5>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}