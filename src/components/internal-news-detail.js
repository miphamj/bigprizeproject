import React, { Component } from 'react';

import './internal-news-detail.scss';

export class InternalNewsDetail extends Component {

    constructor(props) {
        super(props);
    }


    render() {
        const {data} = this.props;
        return (
            <div className="internal-news-detail">
                <img className="info-img" src={data.src} />
                <div className="info">
                    <div>
                        <img src={require('../images/time-black.png')} />
                        <span>{data.time}</span>
                    </div>
                    <h4>{data.title}</h4>
                    <p>{data.summary}</p>
                </div>
            </div>
        )
    }
}