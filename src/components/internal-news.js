import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Row, Col } from 'antd';

import { InternalNewsDetail } from './internal-news-detail';
import './internal-news.scss';

export class InternalNews extends Component {

    constructor(props) {
        super(props);
        this.dataNews = [
            { id: 0, time: '16 tháng 06, 2019', title: 'TAY HO HALF MARATHON 2020', summary: 'Tiền thân của giải TAY HO HALF MARATHON 2020 là giải chạy: “Khám phá Tây Hồ - vì trẻ em có hoàn cảnh đặc biệt khó khăn 2018”', src: require('../images/new05.png'),
                description: `
                <img style='max-height: 600px; object-fit: cover' src="https://images.unsplash.com/photo-1530143311094-34d807799e8f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80" />
                <p style="line-height: 35px; text-align: justify; margin-bottom: 0px">TAY HO HALF MARATHON 2020 là cơ hội tuyệt vời để những người yêu thể thao bắt đầu một năm mới đong đầy sức khỏe, ngập tràn niềm vui. Cung đường chạy quanh Hồ Tây vô cùng thoáng đãng và đặc sắc. Đường chạy đi qua rất nhiều không gian văn hoá của thủ đô Hà Nội, nơi mà thời gian như lắng đọng: đền Quán Thánh, chùa Trấn Quốc, phủ Tây Hồ và vô số danh thắng bậc nhất Hà Thành. Các VĐV hứa hẹn sẽ được tham gia một mùa du xuân chẩy hội theo cách độc đáo chưa từng có. 
                <br/>

                Đây là giải đầu tiên, và duy nhất được UBND Quận Tây Hồ tổ chức, do vậy Quận sẽ thực hiện cấm đường để đảm bảo các VĐV tham gia giải chạy và du xuân độc đáo một cách an toàn nhất.<br/>
                
                Bên cạnh đó, Tết sẽ thêm vui khi ta làm điều ý nghĩa. Mỗi vận động viên tham gia Giải đồng nghĩa với đóng góp 50,000 VND vào Quỹ Trẻ Em quận Tây Hồ. Hãy đến với TAY HO HALF MARATHON 2020 và cùng lan tòa niềm vui ngày Tết đi muôn nơi.</p>
                <img style='max-height:600px; object-fit: cover' src="https://images.unsplash.com/photo-1457449205106-d0aad138e99b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"/>
                <p style="line-height: 35px; text-align: justify;">Hiện tại, EPM 2020 vẫn ở giai đoạn đăng ký Tiêu chuẩn (áp dụng đến 19/12/2019) với các ưu đãi dành cho nhóm.</p>
                <p>Đăng ký: <span style="text-transform: uppercase; color: #ec1f27">Tại đây</span>
                <p>Xem thông tin tại trang web: <a style="font-weight: 600; color: #ec1f27" href="#" target="_blank">https://www.hotayhalfmarathon.com/</a>`
            },
            { id: 1, time: '16 tháng 06, 2019', title: 'Cuộc đua ảo: chạy mọi lúc, mọi nơi', summary: 'Cuộc đua “ảo” Uprace còn là một cột mốc mới để người chạy bộ tiếp tục vượt qua những giới hạn của... ', src: require('../images/finish07.jpg') },
            { id: 2, time: '16 tháng 06, 2019', title: 'Pacer EPM 2019 bật mí bí quyết chạy 42km chuẩn mục tiêu', summary: 'Một giải chạy địa hình ngay tại thủ đô Hà Nội! Không cần phải đi quá xa, trèo đèo lội suối để có một cun...', src: require('../images/finish06.jpg') },
            { id: 3, time: '16 tháng 06, 2019', title: 'Cuộc đua ảo: chạy mọi lúc, mọi nơi', summary: 'Cuộc đua “ảo” Uprace còn là một cột mốc mới để người chạy bộ tiếp tục vượt qua những giới hạn của... ', src: require('../images/finish08.jpg') },
            { id: 4, time: '16 tháng 06, 2019', title: 'Pacer EPM 2019 bật mí bí quyết chạy 42km chuẩn mục tiêu', summary: 'Một giải chạy địa hình ngay tại thủ đô Hà Nội! Không cần phải đi quá xa, trèo đèo lội suối để có một cun...', src: require('../images/finish09.jpg') },
            { id: 5, time: '16 tháng 06, 2019', title: 'Cuộc đua ảo: chạy mọi lúc, mọi nơi', summary: 'Cuộc đua “ảo” Uprace còn là một cột mốc mới để người chạy bộ tiếp tục vượt qua những giới hạn của... ', src: require('../images/news01.jpg') },
            { id: 6, time: '16 tháng 06, 2019', title: 'Pacer EPM 2019 bật mí bí quyết chạy 42km chuẩn mục tiêu', summary: 'Một giải chạy địa hình ngay tại thủ đô Hà Nội! Không cần phải đi quá xa, trèo đèo lội suối để có một cun...', src: require('../images/news02.jpg') },
            { id: 7, time: '16 tháng 06, 2019', title: 'Cuộc đua ảo: chạy mọi lúc, mọi nơi', summary: 'Cuộc đua “ảo” Uprace còn là một cột mốc mới để người chạy bộ tiếp tục vượt qua những giới hạn của... ', src: require('../images/new03.jpg') },
            { id: 8, time: '16 tháng 06, 2019', title: 'Cuộc đua ảo: chạy mọi lúc, mọi nơi', summary: 'Cuộc đua “ảo” Uprace còn là một cột mốc mới để người chạy bộ tiếp tục vượt qua những giới hạn của... ', src: require('../images/new04.jpg') },
        ]
    }

    handleTourDetail = (item) => {
        console.log('item: ', item)
        this.props.history.push("/discovery-detail", { item: item })
    }

    render() {
        console.log('cut arr: ', this.dataNews.slice(1));
        const element = this.dataNews.slice(1).map(item => {
            return (
                <span key={item.id} onClick={() => this.handleTourDetail(item)}>
                    <Col xs={24} lg={6}>
                            <InternalNewsDetail data={item} />
                    </Col>
                </span>
            )
        })
        return (
            <div className="internal-news">
                <Row>
                    <span onClick={() => this.handleTourDetail(this.dataNews[0])}>
                        <Col className="first-news" xs={24} lg={12}>
                                <div className="info">
                                    <div style={{ display: 'flex', alignItems: 'center', marginBottom: '10px' }}>
                                        <img src={require('../images/time.png')} />
                                        <span>{this.dataNews[0].time}</span>
                                    </div>
                                    <h4>{this.dataNews[0].title}</h4>
                                </div>
                                <img className="info-img" src={this.dataNews[0].src} />
                        </Col>
                    </span>
                    {element}
                </Row>
            </div>
        )
    }
}