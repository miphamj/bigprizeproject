import React, { Component } from 'react';

import './intro-title.scss';

export class IntroTitle extends Component {

  constructor(props){
    super(props);
  }
  
  render() {
    const { name } = this.props;
    return (
      <div className="intro-title">
        <p className="title-name">{name}</p>
      </div>
    )
  }
}