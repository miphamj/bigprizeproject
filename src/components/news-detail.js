import React, { Component } from 'react';
import { Card } from 'antd';

import './news-detail.scss';

const  {Meta} = Card;

export class NewsDetail extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        const { item } = this.props;
        return (
            <div className='news-detail'>
                <Card hoverable cover={<img alt="example" src={item.src} />}>
                    <div className="news-time"><img src={require('../images/time-black.png')}/> <span>{item.time}</span></div>
                    <Meta className="news-title" title={item.summary} />
                </Card>
            </div>
        )
    }
}