import React, { Component } from 'react';
import { CategoryTitle } from './category-title';
import { Row, Col } from 'antd';

import { NewsDetail } from './news-detail';
import './news.scss';

export class News extends Component {

    constructor(props) {
        super(props);
        this.dataNews = [
            { id: 0, time: '16 tháng 06, 2019', title: 'Ecopark marathon 2020', summary: 'Giải chạy “Giỗ Tổ Hùng Vương” Ecopark Marathon 2020 đã vượt qua mốc hơn 2500 vận động viên dù tháng 4 năm sau mới diễn ra…', src: require('../images/news01.jpg'),
                description: `
                <img style='max-height: 600px; object-fit: cover' src="https://images.unsplash.com/photo-1530143311094-34d807799e8f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80" />
                <p style="line-height: 35px; text-align: justify; margin-bottom: 0px">Ecopark Marathon 2020 được tổ chức đúng ngày Giỗ Tổ Hùng Vương (10/3 âm) 2/4/2020 tại khu đô thị Ecopark (Hưng Yên). Giải năm tới là mùa giải thứ ba và sẽ có điểm mới khi các vận động viên sẽ lần đầu được chạy trên cung đường bằng với địa hình xen lẫn (city trail).<br/>

                Sau một thời gian ngắn mở đăng ký, EPM 2020 đã vượt qua mốc hơn 2500 vận động viên. Dự đoán, con số này sẽ còn tăng lên chóng mặt và sẽ vượt qua mốc 5000 vận động viên của năm 2019.<br/>
                
                Để tăng tính hấp dẫn cho giải, ban tổ chức sẽ tặng bib chạy cho những vận động viên đăng ký ở thứ tự 1999, 2999, 3999… Người may mắn nằm ở vị trí đăng ký thứ 1999 đã được xác định, đó là anh Văn Thế Chiến (10km) với số bib 6891. Vận động viên này sẽ nhận được 01 bib cùng cự ly hoặc thấp hơn cho người thân, bạn bè…</p>
                <img style='max-height:600px; object-fit: cover' src="https://images.unsplash.com/photo-1457449205106-d0aad138e99b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"/>
                <p style="line-height: 35px; text-align: justify;">Hiện tại, EPM 2020 vẫn ở giai đoạn đăng ký Tiêu chuẩn (áp dụng đến 19/12/2019) với các ưu đãi dành cho nhóm.</p>
                <p>Đăng ký: <span style="text-transform: uppercase; color: #ec1f27">Tại đây</span>
                <p>Xem thông tin tại trang web: <a style="font-weight: 600; color: #ec1f27" href="https://www.ecoparkmarathon.com/" target="_blank">https://www.ecoparkmarathon.com/</a>`
            },
            { id: 1, time: '16 tháng 06, 2019', title: 'Ecopark marathon 2020', summary: 'Giải chạy “Giỗ Tổ Hùng Vương” Ecopark Marathon 2020 đã vượt qua mốc hơn 2500 vận động viên dù tháng 4 năm sau mới diễn ra…', src: require('../images/news02.jpg'),
                description: `
                <img style='max-height: 600px; object-fit: cover' src="https://images.unsplash.com/photo-1530143311094-34d807799e8f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80" />
                <p style="line-height: 35px; text-align: justify; margin-bottom: 0px">Ecopark Marathon 2020 được tổ chức đúng ngày Giỗ Tổ Hùng Vương (10/3 âm) 2/4/2020 tại khu đô thị Ecopark (Hưng Yên). Giải năm tới là mùa giải thứ ba và sẽ có điểm mới khi các vận động viên sẽ lần đầu được chạy trên cung đường bằng với địa hình xen lẫn (city trail).<br/>

                Sau một thời gian ngắn mở đăng ký, EPM 2020 đã vượt qua mốc hơn 2500 vận động viên. Dự đoán, con số này sẽ còn tăng lên chóng mặt và sẽ vượt qua mốc 5000 vận động viên của năm 2019.<br/>
                
                Để tăng tính hấp dẫn cho giải, ban tổ chức sẽ tặng bib chạy cho những vận động viên đăng ký ở thứ tự 1999, 2999, 3999… Người may mắn nằm ở vị trí đăng ký thứ 1999 đã được xác định, đó là anh Văn Thế Chiến (10km) với số bib 6891. Vận động viên này sẽ nhận được 01 bib cùng cự ly hoặc thấp hơn cho người thân, bạn bè…</p>
                <img style='max-height:600px; object-fit: cover' src="https://images.unsplash.com/photo-1457449205106-d0aad138e99b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"/>
                <p style="line-height: 35px; text-align: justify;">Hiện tại, EPM 2020 vẫn ở giai đoạn đăng ký Tiêu chuẩn (áp dụng đến 19/12/2019) với các ưu đãi dành cho nhóm.</p>
                <p>Đăng ký: <span style="text-transform: uppercase; color: #ec1f27">Tại đây</span>
                <p>Xem thông tin tại trang web: <a style="font-weight: 600; color: #ec1f27" href="https://www.ecoparkmarathon.com/" target="_blank">https://www.ecoparkmarathon.com/</a>`
            },
            { id: 2, time: '16 tháng 06, 2019', title: 'Ecopark marathon 2020', summary: 'Giải chạy “Giỗ Tổ Hùng Vương” Ecopark Marathon 2020 đã vượt qua mốc hơn 2500 vận động viên dù tháng 4 năm sau mới diễn ra…', src: require('../images/new03.jpg'),
                description: `
                <img style='max-height: 600px; object-fit: cover' src="https://images.unsplash.com/photo-1530143311094-34d807799e8f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80" />
                <p style="line-height: 35px; text-align: justify; margin-bottom: 0px">Ecopark Marathon 2020 được tổ chức đúng ngày Giỗ Tổ Hùng Vương (10/3 âm) 2/4/2020 tại khu đô thị Ecopark (Hưng Yên). Giải năm tới là mùa giải thứ ba và sẽ có điểm mới khi các vận động viên sẽ lần đầu được chạy trên cung đường bằng với địa hình xen lẫn (city trail).<br/>

                Sau một thời gian ngắn mở đăng ký, EPM 2020 đã vượt qua mốc hơn 2500 vận động viên. Dự đoán, con số này sẽ còn tăng lên chóng mặt và sẽ vượt qua mốc 5000 vận động viên của năm 2019.<br/>
                
                Để tăng tính hấp dẫn cho giải, ban tổ chức sẽ tặng bib chạy cho những vận động viên đăng ký ở thứ tự 1999, 2999, 3999… Người may mắn nằm ở vị trí đăng ký thứ 1999 đã được xác định, đó là anh Văn Thế Chiến (10km) với số bib 6891. Vận động viên này sẽ nhận được 01 bib cùng cự ly hoặc thấp hơn cho người thân, bạn bè…</p>
                <img style='max-height:600px; object-fit: cover' src="https://images.unsplash.com/photo-1457449205106-d0aad138e99b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"/>
                <p style="line-height: 35px; text-align: justify;">Hiện tại, EPM 2020 vẫn ở giai đoạn đăng ký Tiêu chuẩn (áp dụng đến 19/12/2019) với các ưu đãi dành cho nhóm.</p>
                <p>Đăng ký: <span style="text-transform: uppercase; color: #ec1f27">Tại đây</span>
                <p>Xem thông tin tại trang web: <a style="font-weight: 600; color: #ec1f27" href="https://www.ecoparkmarathon.com/" target="_blank">https://www.ecoparkmarathon.com/</a>`
            },
            { id: 3, time: '16 tháng 06, 2019', title: 'Ecopark marathon 2020', summary: 'Giải chạy “Giỗ Tổ Hùng Vương” Ecopark Marathon 2020 đã vượt qua mốc hơn 2500 vận động viên dù tháng 4 năm sau mới diễn ra…', src: require('../images/new04.jpg'),
                description: `
                <img style='max-height: 600px; object-fit: cover' src="https://images.unsplash.com/photo-1530143311094-34d807799e8f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80" />
                <p style="line-height: 35px; text-align: justify; margin-bottom: 0px">Ecopark Marathon 2020 được tổ chức đúng ngày Giỗ Tổ Hùng Vương (10/3 âm) 2/4/2020 tại khu đô thị Ecopark (Hưng Yên). Giải năm tới là mùa giải thứ ba và sẽ có điểm mới khi các vận động viên sẽ lần đầu được chạy trên cung đường bằng với địa hình xen lẫn (city trail).<br/>

                Sau một thời gian ngắn mở đăng ký, EPM 2020 đã vượt qua mốc hơn 2500 vận động viên. Dự đoán, con số này sẽ còn tăng lên chóng mặt và sẽ vượt qua mốc 5000 vận động viên của năm 2019.<br/>
                
                Để tăng tính hấp dẫn cho giải, ban tổ chức sẽ tặng bib chạy cho những vận động viên đăng ký ở thứ tự 1999, 2999, 3999… Người may mắn nằm ở vị trí đăng ký thứ 1999 đã được xác định, đó là anh Văn Thế Chiến (10km) với số bib 6891. Vận động viên này sẽ nhận được 01 bib cùng cự ly hoặc thấp hơn cho người thân, bạn bè…</p>
                <img style='max-height:600px; object-fit: cover' src="https://images.unsplash.com/photo-1457449205106-d0aad138e99b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"/>
                <p style="line-height: 35px; text-align: justify;">Hiện tại, EPM 2020 vẫn ở giai đoạn đăng ký Tiêu chuẩn (áp dụng đến 19/12/2019) với các ưu đãi dành cho nhóm.</p>
                <p>Đăng ký: <span style="text-transform: uppercase; color: #ec1f27">Tại đây</span>
                <p>Xem thông tin tại trang web: <a style="font-weight: 600; color: #ec1f27" href="https://www.ecoparkmarathon.com/" target="_blank">https://www.ecoparkmarathon.com/</a>`
            },
        ]
    }

    handleNewDetail = (item) => {
        this.props.history.push("/discovery-detail", {item: item});
    }

    render() {
        const width = window.innerWidth;
        const element = this.dataNews.map(item => {
            return (
                <span key={item.id} onClick={() => this.handleNewDetail(item)}>
                    <NewsDetail item={item} />
                </span>
            )
        });
        return (
            <div className='news'>
                <CategoryTitle name="Tin tức" />
                <div className="news-list" style={{width: `${width}`}}>
                        {element}
                </div>
                <div className="btn-more">
                    <div><button>Xem tất cả</button></div>
                </div>
            </div>
        )
    }
}