import React, { Component } from 'react';
import { Menu } from 'antd';

export class RightMenu extends Component {
  render() {
    return (
      <Menu mode="horizontal">
        <Menu.Item key="home">
          <a href="#">Trang chủ</a>
        </Menu.Item>
        <Menu.Item key="intro">
          <a href="#">Giới thiệu</a>
        </Menu.Item>
      </Menu>
    );
  }
}