import React, { Component } from 'react';
import { Tabs, Table } from 'antd';

import { BtnSlash } from "./btn-slash";

import './tour-detail.scss'

const { TabPane } = Tabs;

export class TourDetail extends Component {

    constructor(props) {
        super(props);
    }

    callback(key) {
    }

    render() {
        const { infoDetail } = this.props;
        console.log('data source: ', this.props.infoDetail.registerInfo);
        const columnsPrice = [
            {
                title: "Cự ly",
                dataIndex: 'distance',
                key: 'distance',
            },
            {
                title: () => <div>Early Bird (999BIB)<br /> {infoDetail.registerExpiredDate.EPMEarlyBird}</div>,
                dataIndex: 'earlyBird',
                key: 'earlyBird',
                render: earlyBird => <span style={{ color: "#669938", fontWeight: '700' }}>{earlyBird}</span>
            },
            {
                title: () => <div>Standard<br /> {infoDetail.registerExpiredDate.EPMStandard}</div>,
                dataIndex: 'standard',
                key: 'standard',
                render: standard => <span style={{ color: "#669938", fontWeight: '700' }}>{standard}</span>
            },
            {
                title: () => <div>Late<br /> {infoDetail.registerExpiredDate.EPMLate}</div>,
                dataIndex: 'late',
                key: 'late',
                render: standard => <span style={{ color: "#669938", fontWeight: '700' }}>{standard}</span>
            },
        ]
        const columnsDiscount = [
            {
                title: "Group",
                dataIndex: "group",
                key: "group"
            },
            {
                title: "Discount",
                dataIndex: "discount",
                key: "discount"
            },
        ]

        return (
            <div className="tour-detail">
                <Tabs className="tabs" defaultActiveKey="1" onChange={this.callback}>
                    <TabPane tab="Thông tin giải đấu" key="1">
                        <h4 className="title">Thông tin giải đấu</h4>
                        <h5 className="tour-title">
                            {infoDetail.tournamentInfo.title}
                        </h5>
                        <div className="tour-description">
                            <div dangerouslySetInnerHTML={{ __html: `${infoDetail.tournamentInfo.description}` }} ></div>
                        </div>

                        <div className="register-tour">
                            <h4 className="title">Thông tin đăng ký</h4>
                            <h5 className="title">Price policy</h5>
                            <Table dataSource={infoDetail.registerInfo} columns={columnsPrice} />
                            <h5 className="title">Discount policy</h5>
                        </div>
                        <div className="register-rule">
                            <div dangerouslySetInnerHTML={{ __html: `${infoDetail.registerRule}` }} ></div>
                        </div>
                        <Table className="discountPolicy" dataSource={infoDetail.groupDiscount} columns={columnsDiscount} />
                        <div className="register-now">
                            <div className="register">
                                <h2>Đăng ký ngay để không bỏ lỡ cuộc đua!</h2>
                                <BtnSlash name="Đăng ký ngay" />
                            </div>
                            <img src={window.innerWidth < 786 ? require('../images/register-mobile.jpg') : require('../images/footer-tourdetail.jpg')} />
                        </div>
                        <div className="share">
                            Chia sẻ: <img src={require('../images/share-fb.png')} /> <img src={require('../images/share-tw.png')} />
                        </div>
                    </TabPane>
                    {/* <TabPane tab="Điều kiện tham gia" key="2">
                        Content of Tab Pane 2
                    </TabPane> */}
                </Tabs>
                <div className="related-tour">

                </div>
            </div>
        )
    }
}