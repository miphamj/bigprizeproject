import React, { Component } from 'react';
import { Col, Row } from 'antd';
import { Link } from 'react-router-dom';

import { Tournament } from './tournament';
import { Button } from 'antd';
import ItemsCarousel from 'react-items-carousel';

import './tournament-organization.scss';

export class TournamentOrganization extends Component {

    constructor(props) {
        super(props);
        this.dataComing = [
            {
                id: 1,
                name: 'TAY HO HALF MARATHON 2020', 
                description: 'Nhiều cự ly chạy, đa dạng. phù hợp với cả những VĐV đã tập chạy lâu năm và cả với các bạn mới tập chạy, các trẻ nhỏ đối tượng', 
                add: 'Sân bóng An Dương, Tây Hồ, Hà Nội', 
                time: '16/02/2020 04:30', distance: '3 km | 6 km | 15 km | 21 km', 
                src: require('../images/bg09.jpg'), 
                banner: require('../images/bg08.jpg'),
                expriedDate: '20/05/2020',
                tournamentInfo: {
                    title: 'TAY HO HALF MARATHON 2020 là cơ hội tuyệt vời để những người yêu thể thao bắt đầu một năm mới đong đầy sức khỏe, ngập tràn niềm vui.',
                    description: `<p>Cung đường chạy quanh Hồ Tây vô cùng thoáng đãng và đặc sắc. Đường chạy đi qua rất nhiều không gian văn hoá của thủ đô Hà Nội, nơi mà thời 
                    gian như lắng đọng: đền Quán Thánh, chùa Trấn Quốc, phủ Tây Hồ và vô số danh thắng bậc nhất Hà Thành. Các VĐV hứa hẹn sẽ được tham gia một mùa du xuân chẩy 
                    hội theo cách độc đáo chưa từng có. </p>
                    <p>Đây là giải đầu tiên, và duy nhất được UBND Quận Tây Hồ tổ chức, do vậy Quận sẽ thực hiện cấm đường để đảm bảo các VĐV tham gia giải chạy và du xuân độc 
                    đáo một cách an toàn nhất. </p>
                    <p>Bên cạnh đó, Tết sẽ thêm vui khi ta làm điều ý nghĩa. Mỗi vận động viên tham gia Giải đồng nghĩa với đóng góp 50,000 VND vào Quỹ Trẻ Em quận Tây Hồ. Hãy 
                    đến với TAY HO HALF MARATHON 2020 và cùng lan tòa niềm vui ngày Tết đi muôn nơi.</p>`
                },
                registerInfo: [
                    {
                        key: '1',
                        distance: '21km',
                        earlyBird: '650.000',
                        standard: '799.000',
                        late: '1.250.000'
                    },
                    {
                        key: '2',
                        distance: '15km',
                        earlyBird: '550.000',
                        standard: '699.000',
                        late: '1.150.000'
                    },
                    {
                        key: '3',
                        distance: '6km',
                        earlyBird: '450.000',
                        standard: '599.000',
                        late: '900.000'
                    },
                    {
                        key: '4',
                        distance: '3km',
                        earlyBird: '350.000',
                        standard: '499.000',
                        late: '700.000'
                    },
                ],
                registerExpiredDate: {
                    EPMPriorit: '19.9-21.9',
                    EPMSuperEarlyBird: '22.9-29.9',
                    EPMEarlyBird: '30.9-20.10',
                    EPMStandard: 'until 31.12',
                    EPMLate: '01.01.19-15.01.20'
                },
                registerRule: `<span style="font-weight: 700">*</span> Family Ticket: 01 bố + 01 mẹ + 01 trẻ dưới 12 tuổi, giảm 10% tổng hóa đơn<br/>
                <span style="font-weight: 700">*</span> Group Discount: `,
                groupDiscount: [
                    {
                        key: '1',
                        group: '10 people',
                        discount: '5%',
                    },
                    {
                        key: '2',
                        group: '30 people',
                        discount: '8%',
                    },
                    {
                        key: '3',
                        group: '60 people',
                        discount: '12%',
                    },
                    {
                        key: '4',
                        group: '100  people and more',
                        discount: '17%',
                    },
                ]
                
            },
            { 
                id: 2, name: 'Template Marathon 2020', 
                description: 'Nhiều cự ly chạy, đa dạng. phù hợp với cả những VĐV đã tập chạy lâu năm và cả với các bạn mới tập chạy, các trẻ nhỏ đối tượng', 
                add: 'Hà Nội', 
                time: '16/02/2020 04:30', distance: '3 km | 6 km | 15 km | 21 km', 
                src: require('../images/template.png'), 
                banner: require('../images/template.png'),
                expriedDate: '20/05/2020',
                tournamentInfo: {
                    title: 'TAY HO HALF MARATHON 2020 là cơ hội tuyệt vời để những người yêu thể thao bắt đầu một năm mới đong đầy sức khỏe, ngập tràn niềm vui.',
                    description: `<p>Cung đường chạy quanh Hồ Tây vô cùng thoáng đãng và đặc sắc. Đường chạy đi qua rất nhiều không gian văn hoá của thủ đô Hà Nội, nơi mà thời 
                    gian như lắng đọng: đền Quán Thánh, chùa Trấn Quốc, phủ Tây Hồ và vô số danh thắng bậc nhất Hà Thành. Các VĐV hứa hẹn sẽ được tham gia một mùa du xuân chẩy 
                    hội theo cách độc đáo chưa từng có. </p>
                    <p>Đây là giải đầu tiên, và duy nhất được UBND Quận Tây Hồ tổ chức, do vậy Quận sẽ thực hiện cấm đường để đảm bảo các VĐV tham gia giải chạy và du xuân độc 
                    đáo một cách an toàn nhất. </p>
                    <p>Bên cạnh đó, Tết sẽ thêm vui khi ta làm điều ý nghĩa. Mỗi vận động viên tham gia Giải đồng nghĩa với đóng góp 50,000 VND vào Quỹ Trẻ Em quận Tây Hồ. Hãy 
                    đến với TAY HO HALF MARATHON 2020 và cùng lan tòa niềm vui ngày Tết đi muôn nơi.</p>`
                },
                registerInfo: [
                    {
                        key: '1',
                        distance: 'FM 42km',
                        priority: 'Sold out',
                        superEarlyBird: 'Sold out',
                        earlyBird: 'Soldout',
                        standard: '999.000',
                        late: '1.290.000'
                    },
                    {
                        key: '2',
                        distance: 'HM 21km',
                        priority: 'Sold out',
                        superEarlyBird: 'Sold out',
                        earlyBird: 'Soldout',
                        standard: '999.000',
                        late: '1.290.000'
                    },
                    {
                        key: '3',
                        distance: '10km',
                        priority: 'Sold out',
                        superEarlyBird: 'Sold out',
                        earlyBird: 'Soldout',
                        standard: '999.000',
                        late: '1.290.000'
                    },
                    {
                        key: '4',
                        distance: '5km',
                        priority: 'Sold out',
                        superEarlyBird: 'Sold out',
                        earlyBird: 'Soldout',
                        standard: '999.000',
                        late: '1.290.000'
                    },
                    {
                        key: '5',
                        distance: '5km Kids run (<12 years)',
                        priority: 'Sold out',
                        superEarlyBird: 'Sold out',
                        earlyBird: 'Soldout',
                        standard: '999.000',
                        late: '1.290.000'
                    },
                ],
                registerExpiredDate: {
                    EPMPriorit: '19.9-21.9',
                    EPMSuperEarlyBird: '22.9-29.9',
                    EPMEarlyBird: '30.9-20.10',
                    EPMStandard: 'until 31.12',
                    EPMLate: '01.01.19-15.01.20'
                },
                 registerRule: `<span style="font-weight: 700">* Family Ticket: 01 bố + 01 mẹ + 01 trẻ dưới 12 tuổi, giảm 10% tổng hóa đơn</span>`
                
            },
            { 
                id: 3, name: 'Template Marathon 3rd Ekiden', 
                description: 'Nhiều cự ly chạy, đa dạng. phù hợp với cả những VĐV đã tập chạy lâu năm và cả với các bạn mới tập chạy, các trẻ nhỏ đối tượng', 
                add: 'Hà Nội', 
                time: '16/02/2020 04:30', distance: '3 km | 6 km | 15 km | 21 km', 
                src: require('../images/template.png'), 
                banner: require('../images/template.png'),
                expriedDate: '20/05/2020',
                tournamentInfo: {
                    title: 'TAY HO HALF MARATHON 2020 là cơ hội tuyệt vời để những người yêu thể thao bắt đầu một năm mới đong đầy sức khỏe, ngập tràn niềm vui.',
                    description: `<p>Cung đường chạy quanh Hồ Tây vô cùng thoáng đãng và đặc sắc. Đường chạy đi qua rất nhiều không gian văn hoá của thủ đô Hà Nội, nơi mà thời 
                    gian như lắng đọng: đền Quán Thánh, chùa Trấn Quốc, phủ Tây Hồ và vô số danh thắng bậc nhất Hà Thành. Các VĐV hứa hẹn sẽ được tham gia một mùa du xuân chẩy 
                    hội theo cách độc đáo chưa từng có. </p>
                    <p>Đây là giải đầu tiên, và duy nhất được UBND Quận Tây Hồ tổ chức, do vậy Quận sẽ thực hiện cấm đường để đảm bảo các VĐV tham gia giải chạy và du xuân độc 
                    đáo một cách an toàn nhất. </p>
                    <p>Bên cạnh đó, Tết sẽ thêm vui khi ta làm điều ý nghĩa. Mỗi vận động viên tham gia Giải đồng nghĩa với đóng góp 50,000 VND vào Quỹ Trẻ Em quận Tây Hồ. Hãy 
                    đến với TAY HO HALF MARATHON 2020 và cùng lan tòa niềm vui ngày Tết đi muôn nơi.</p>`
                },
                registerInfo: [
                    {
                        key: '1',
                        distance: 'FM 42km',
                        priority: 'Sold out',
                        superEarlyBird: 'Sold out',
                        earlyBird: 'Soldout',
                        standard: '999.000',
                        late: '1.290.000'
                    },
                    {
                        key: '2',
                        distance: 'HM 21km',
                        priority: 'Sold out',
                        superEarlyBird: 'Sold out',
                        earlyBird: 'Soldout',
                        standard: '999.000',
                        late: '1.290.000'
                    },
                    {
                        key: '3',
                        distance: '10km',
                        priority: 'Sold out',
                        superEarlyBird: 'Sold out',
                        earlyBird: 'Soldout',
                        standard: '999.000',
                        late: '1.290.000'
                    },
                    {
                        key: '4',
                        distance: '5km',
                        priority: 'Sold out',
                        superEarlyBird: 'Sold out',
                        earlyBird: 'Soldout',
                        standard: '999.000',
                        late: '1.290.000'
                    },
                    {
                        key: '5',
                        distance: '5km Kids run (<12 years)',
                        priority: 'Sold out',
                        superEarlyBird: 'Sold out',
                        earlyBird: 'Soldout',
                        standard: '999.000',
                        late: '1.290.000'
                    },
                ],
                registerExpiredDate: {
                    EPMPriorit: '19.9-21.9',
                    EPMSuperEarlyBird: '22.9-29.9',
                    EPMEarlyBird: '30.9-20.10',
                    EPMStandard: 'until 31.12',
                    EPMLate: '01.01.19-15.01.20'
                },
                 registerRule: `<span style="font-weight: 700">* Family Ticket: 01 bố + 01 mẹ + 01 trẻ dưới 12 tuổi, giảm 10% tổng hóa đơn</span>`
                
            },
        ];
        this.dataFinished = [
            { id: 1, name: 'HANOI CITY TRAIL RUN 2020', description: 'Một giải chạy địa hình ngay tại thủ đô Hà Nội! Không cần phải đi quá xa, trèo đèo lội suối để... ', add: 'Sân bóng An Dương, Tây Hồ, Hà Nội', time: '01/01/2020 04:30', distance: '5 km | 10 km | 15 km', src: require('../images/finish01.jpg') },
            { id: 2, name: 'Sunset Bay Triathlon 2019', description: 'Một giải chạy địa hình ngay tại thủ đô Hà Nội! Không cần phải đi quá xa, trèo đèo lội suối để... ', add: 'Sân bóng An Dương, Tây Hồ, Hà Nội', time: '01/01/2020 04:30', distance: '5 km | 10 km | 15 km', src: require('../images/finish02.jpg') },
            { id: 3, name: 'Longbien Marathon 3rd Ekiden', description: 'Một giải chạy địa hình ngay tại thủ đô Hà Nội! Không cần phải đi quá xa, trèo đèo lội suối để... ', add: 'Sân bóng An Dương, Tây Hồ, Hà Nội', time: '01/01/2020 04:30', distance: '5 km | 10 km | 15 km', src: require('../images/finish03.jpg') },
            { id: 4, name: 'HANOI CITY TRAIL RUN 2020', description: 'Một giải chạy địa hình ngay tại thủ đô Hà Nội! Không cần phải đi quá xa, trèo đèo lội suối để... ', add: 'Sân bóng An Dương, Tây Hồ, Hà Nội', time: '01/01/2020 04:30', distance: '5 km | 10 km | 15 km', src: require('../images/finish04.jpg') },
            { id: 5, name: 'Sunset Bay Triathlon 2019', description: 'Một giải chạy địa hình ngay tại thủ đô Hà Nội! Không cần phải đi quá xa, trèo đèo lội suối để... ', add: 'Sân bóng An Dương, Tây Hồ, Hà Nội', time: '01/01/2020 04:30', distance: '5 km | 10 km | 15 km', src: require('../images/finish05.jpg') },
            { id: 6, name: 'Longbien Marathon 3rd Ekiden', description: 'Một giải chạy địa hình ngay tại thủ đô Hà Nội! Không cần phải đi quá xa, trèo đèo lội suối để... ', add: 'Sân bóng An Dương, Tây Hồ, Hà Nội', time: '01/01/2020 04:30', distance: '5 km | 10 km | 15 km', src: require('../images/finish06.jpg') },
            { id: 7, name: 'HANOI CITY TRAIL RUN 2020', description: 'Một giải chạy địa hình ngay tại thủ đô Hà Nội! Không cần phải đi quá xa, trèo đèo lội suối để... ', add: 'Sân bóng An Dương, Tây Hồ, Hà Nội', time: '01/01/2020 04:30', distance: '5 km | 10 km | 15 km', src: require('../images/finish07.jpg') },
            { id: 8, name: 'Sunset Bay Triathlon 2019', description: 'Một giải chạy địa hình ngay tại thủ đô Hà Nội! Không cần phải đi quá xa, trèo đèo lội suối để... ', add: 'Sân bóng An Dương, Tây Hồ, Hà Nội', time: '01/01/2020 04:30', distance: '5 km | 10 km | 15 km', src: require('../images/finish08.jpg') },
            { id: 9, name: 'Longbien Marathon 3rd Ekiden', description: 'Một giải chạy địa hình ngay tại thủ đô Hà Nội! Không cần phải đi quá xa, trèo đèo lội suối để... ', add: 'Sân bóng An Dương, Tây Hồ, Hà Nội', time: '01/01/2020 04:30', distance: '5 km | 10 km | 15 km', src: require('../images/finish09.jpg') },
        ];
        this.state = {
            activeItemIndex: 0,
        }
    }

    handleTourDetail = (item) => {
        this.props.history.push("/tournament-detail", {item: item});
    }

    render() {
        const { activeItemIndex } = this.state;
        const chevronWidth = 40;
        const noOfCards = window.innerWidth >= 992 ? 3 : window.innerWidth >= 768 ? 2 : 1;
        console.log('this.dataComing: ', this.dataComing)
        const coming = this.dataComing.map(item => {
            return (
                <span key={item.id} onClick={() => this.handleTourDetail(item)}>
                    <Tournament item={item} background="red" />
                </span>
            )
        });
        const coming2 = this.dataComing.map(item => {
            return (
                <Col onClick={() => this.handleTourDetail(item)} className="mobile-item" key={item.id} xs={24} md={8}>
                    <Tournament item={item} background="red" />
                </Col>
            )
        });
        const finished = this.dataFinished.map(item => {
            return (
                <Col key={item.id} xs={24} md={8}>
                    <Tournament item={item} background="white" />
                </Col>
            )
        })
        return (
            <div className="tournament-organization">
                <section className="blank-space"></section>
                <div className="tournament-organization-background">
                    <div className="tournament-organization-coming">
                        <h2>Giải đấu sắp diễn ra</h2>
                        <span className="large-src">
                            <ItemsCarousel
                                infiniteLoop
                                gutter={12}
                                numberOfCards={noOfCards}
                                activeItemIndex={activeItemIndex}
                                requestToChangeActive={index => {
                                    this.setState({ activeItemIndex: index })
                                }}
                                rightChevron={
                                    <Button className="btn-chevron-right">
                                        <img src={require('../images/right-arrow.jpg')} />
                                    </Button>
                                }
                                leftChevron={
                                    <Button className="btn-chevron-left">
                                        <img src={require('../images/left-arrow.jpg')} />
                                    </Button>
                                }
                                outsideChevron
                                chevronWidth={chevronWidth}
                            >
                                {coming}
                            </ItemsCarousel>
                        </span>

                        <Row className="mobile-scr">
                            {coming2}
                        </Row>
                    </div>
                </div>
                <section className="blank-space"></section>
                <div className="tournament-organization-finished">
                    <h2>Giải đấu đã hoàn thành</h2>
                    <Row>{finished}</Row>
                </div>
                <div className="btn-more">
                    <div><button>Hiển thị thêm</button></div>
                </div>
            </div>
        )
    }
}