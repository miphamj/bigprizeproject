import React, { Component } from 'react';
import { Card } from 'antd';
import { Link } from 'react-router-dom';

import './tournament.scss';

const { Meta } = Card;

export class Tournament extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { item, background } = this.props;
        return (
            <div className={background === 'red' ? "tournament-red" : "tournament-white"}>
                <Card hoverable cover={<img alt="example" src={item.src} />}>
                    <Meta className="tournament-title" title={item.name} />
                    <div className="tournament-content">
                        <p>{item.description}</p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><img src={background === 'red' ? require('../images/location.png') : require('../images/location-black.png')} /></td>
                                    <td>{item.add}</td>
                                </tr>
                                <tr>
                                    <td><img src={background === 'red' ? require('../images/time.png') : require('../images/time-black.png')} /></td>
                                    <td>{item.time}</td>
                                </tr>
                                <tr>
                                    <td><img src={background === 'red' ? require('../images/distance.png') : require('../images/distance_black.png')} /></td>
                                    <td>{item.distance}</td>
                                </tr>
                                <tr>
                                    <td><img src={background === 'red' ? require('../images/info.png') : require('../images/info-red.png')} /></td>
                                    <td><Link className="tournament-info" to='/'>Thông tin giải đấu</Link></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </Card>
            </div>
        )
    }
}