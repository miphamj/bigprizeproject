import React, { Component } from 'react'
import { Button } from 'antd';

export class Home extends Component {
    render () {
        return (
            <div className="container">
                <Button>
                    Home
                </Button>
            </div>
        )
    }
}
