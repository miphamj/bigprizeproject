import React, { Component } from 'react';
import { Col, Row, Form, Icon, Input } from 'antd';
import GoogleMapReact from 'google-map-react'

import { Header } from '../../components/header';
import { HeaderCarousel } from '../../components/carousel';
import { Footer } from '../../components/footer';
import { CategoryTitle } from '../../components/category-title';
import { BtnSlash } from '../../components/btn-slash';

import './contact.scss';

const AnyReactComponent = () => <div><img style={{maxWidth: 20}} src={require('../../images/map-marker.png')}/></div>;
const { TextArea } = Input;
export default class Contact extends Component {

    constructor(props) {
        super(props);
        this.bgImg = [require('../../images/bg01.jpg'), require('../../images/bg06.jpg'), require('../../images/bg05.jpg')];
    }

    static defaultProps = {
        center: { lat: 20.997210, lng: 105.803930 },
        zoom: 16
    }

    render() {
        return (
            <div className="contact">
                <Header pathName="/contact" />
                <HeaderCarousel height="500" bgImg={this.bgImg} />
                <div className="content">
                    <CategoryTitle name="Liên hệ" />
                    <p className="note">Bạn có thể liên hệ trực tiếp với chúng tôi hoặc có thể điền thông tin vào form bên dưới. Chúng tôi sẽ sớm trả lời liên hệ của bạn.</p>
                    <div className="info-contact">
                        <Row>
                            <Col xs={24} md={9} className="info-contact-item">
                                <div className="icon">
                                    <img src={require('../../images/pin.png')} />
                                </div>
                                <div className="text">
                                    <h5>Địa chỉ công ty</h5>
                                    <p>Tầng 6, toà nhà An Huy, số 184,  đường Nguyễn Tuân, Thanh Xuân, Hà Nội.</p>
                                </div>
                            </Col>
                            <Col xs={24} md={7} className="info-contact-item">
                                <div className="icon">
                                    <img src={require('../../images/phone.png')} />
                                </div>
                                <div className="text">
                                    <h5>Điện thoại</h5>
                                    <p>093 442 4400</p>
                                </div>
                            </Col>
                            <Col xs={24} md={8} className="info-contact-item">
                                <div className="icon">
                                    <img src={require('../../images/mail.png')} />
                                </div>
                                <div className="text">
                                    <h5>Email</h5>
                                    <p>contact@bigprize.vn</p>
                                </div>
                            </Col>
                        </Row>
                    </div>
                    <div className="form-contact">
                        <Form>
                            <Row>
                                <Col xs={24} md={12}>
                                    <Form.Item>
                                        <label>Họ và tên</label>
                                        <Input
                                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                            placeholder="Nhập tên đầy đủ của bạn"
                                        />
                                    </Form.Item>
                                    <Form.Item>
                                        <label>Email</label>
                                        <Input
                                            prefix={<Icon type="phone" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                            placeholder="Email của bạn"
                                        />
                                    </Form.Item>
                                    <Form.Item>
                                        <label>Số điện thoại</label>
                                        <Input
                                            prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                            placeholder="Số điện thoại của bạn"
                                        />
                                    </Form.Item>
                                </Col>
                                <Col xs={24} md={12}>
                                    <Form.Item>
                                        <label>Tiêu đề</label>
                                        <Input
                                            prefix={<Icon type="form" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                            placeholder="Nhập tiêu đề liên hệ"
                                        />
                                    </Form.Item>
                                    <Form.Item>
                                        <label>Nội dung</label>
                                        <TextArea rows={5}
                                            placeholder="Nội dung liên hệ"
                                        />
                                    </Form.Item>
                                </Col>
                            </Row>
                            <div className="ct-btn">
                                <BtnSlash name="Gửi" />
                            </div>
                        </Form>
                    </div>

                    <div style={{ height: '600px', width: '100%' }}>
                        <GoogleMapReact
                            bootstrapURLKeys={{ key: 'AIzaSyDloJCMYtTjy_H4hGHYnbMx9GiYWSM8UJ4' }}
                            defaultCenter={this.props.center}
                            defaultZoom={this.props.zoom}
                        >
                            <AnyReactComponent
                                lat={20.997210}
                                lng={105.803930}
                                text="An & Huy Building"
                            />
                        </GoogleMapReact>
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}