import React, { Component } from 'react';

import './discovery-detail.scss'
import { Header } from '../../components/header'
import { HeaderCarousel } from '../../components/carousel';
import { Footer } from '../../components/footer';

export class DiscoveryDetail extends Component {

    constructor(props) {
        super(props);
        this.bgImg = [require('../../images/bg01.jpg'), require('../../images/bg06.jpg'), require('../../images/bg05.jpg')];
    }

    render() {
        const { item } = this.props.history.location.state;
        console.log('history data: ', this.props.history.location.state);
        return (
            <div className="discovery-detail">
                <Header pathName="/news" />
                <HeaderCarousel height="500" bgImg={this.bgImg} />
                <div className="content">
                     <h4>{item.title}</h4>
                    <div className="time">
                        <img src={require('../../images/time-black.png')} />
                        <span>{item.time}</span>
                    </div>
                    <div className='summary'>{item.summary}</div>
                    <div  className="illustraton">
                        <div dangerouslySetInnerHTML={{__html: `${item.description}`}} ></div>
                    </div>
                    
                </div>
                <Footer />
            </div>
        )
    }
}