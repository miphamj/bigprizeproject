import React, { Component } from 'react';
import { Tabs } from 'antd';

import { Header } from '../../components/header';
import { HeaderCarousel } from '../../components/carousel';
import { TournamentOrganization } from '../../components/tournament-organization';
import { Footer } from '../../components/footer';
import { CategoryTitle } from '../../components/category-title';
import { InternalNews } from '../../components/internal-news'

import './discovery.scss';

const { TabPane } = Tabs;
export class Discovery extends Component {

    constructor(props) {
        super(props);
        this.bgImg = [require('../../images/bg01.jpg'),require('../../images/bg06.jpg'),require('../../images/bg05.jpg')];
    }

    callback(key) {
        
    }

    render() {
        return (
            <div className="discovery">
                <Header pathName="/news" />
                <HeaderCarousel height="500" bgImg={this.bgImg} />
                <div className="content">
                    <CategoryTitle name="Tin tức" />
                    <Tabs className="tabs" defaultActiveKey="1" onChange={this.callback}>
                        <TabPane tab="Tin nội bộ" key="1">
                            <InternalNews {...this.props} />
                        </TabPane>
                        <TabPane tab="Tin tuyển dụng" key="2">
                            Content of Tab Pane 2
                        </TabPane>
                    </Tabs>
                </div>
                <Footer />
            </div>
        )
    }
}