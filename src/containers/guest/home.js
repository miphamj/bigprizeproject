import React, { Component } from 'react';

import './home.scss'
import { Header } from '../../components/header'
import { HeaderCarousel } from '../../components/carousel';
import { ComingSoon } from '../../components/coming-soon';
import { CompleteEvent } from '../../components/complete-event';
import { Impressive } from '../../components/impressive';
import { News } from '../../components/news';
import { Connection } from '../../components/connection';
import { Footer } from '../../components/footer';
import { thisExpression } from '@babel/types';
export class Home extends Component {

  constructor(props){
    super(props);
    this.bgImg = [require('../../images/bg01.jpg'),require('../../images/bg03.jpg'),require('../../images/bg05.jpg')];
  }
  
  render() {
    return (
      <div className="container">
        <Header pathName="/home" />
        <HeaderCarousel bgImg={this.bgImg} />
        <ComingSoon {...this.props} />
        <CompleteEvent />
        <Impressive />
        <News {...this.props} />
        <Connection />
        <Footer />
      </div>
    )
  }
}