import React, { Component } from 'react';
import { Tabs } from 'antd';

import './introduce.scss'
import { Header } from '../../components/header'
import { HeaderCarousel } from '../../components/carousel';
import { Footer } from '../../components/footer';
import { CategoryTitle } from '../../components/category-title';
import { IntroTitle } from '../../components/intro-title';

const { TabPane } = Tabs;

export class Introduce extends Component {

  constructor(props) {
    super(props);
    this.bgImg = [require('../../images/bg01.jpg'), require('../../images/bg06.jpg'), require('../../images/bg05.jpg')];
  }

  callback = key => {
    console.log(key);
  }

  render() {
    return (
      <div className="introduce" id="intro">
        <Header pathName="/introduce" />
        <HeaderCarousel height="500" bgImg={this.bgImg} />
        <div className="content">
          <CategoryTitle name="Giới thiệu" />
          <div className="introduce-tabs">
            <Tabs defaultActiveKey="1" tabPosition="left" onChange={this.callback}>
              <TabPane tab="Về Big Prize" key={1}>
                <div className="whoWeAre">
                  <div>
                    <IntroTitle name="Chúng tôi là" />
                    <p className="highlight">Công ty Cổ phần Big Prize được thành lập và phát triển với sứ mệnh nâng cao thể lực và lối sống lành mạnh của người Việt Nam thông qua
                việc phát triển phong trào chạy bộ cộng đồng.</p>
                    <p>“Sức khỏe là món quà lớn nhất dành cho mọi người” là tôn chỉ hoạt động của chúng tôi. Được sáng lập và vận hành bởi các nhà tổ chức sự kiện chuyên nghiệp đồng
                    thời cũng là các runners nhiều kinh nghiệm chinh phục các giải Marathon trong và ngoài nước, Big Prize luôn mong muốn mang tới những giải chạy chuyên nghiệp, đẳng
                 cấp và độc đáo cho cộng đồng.</p>
                    <IntroTitle name="Các thành tựu" />
                    <p>Big Prize đã tổ chức thành công nhiều giải chạy, thu hút được hàng nghìn VĐV trong nước và ngoài nước tham gia, giới thiệu được nhiều cảnh tuyệt sắc và nhiều
                    địa danh của Việt Nam tới với khách du lịch trong nước và Quốc tế. Một số giải chạy của Big Prize đã được các vận động viên tham gia đánh giá rất cao như: Tràng
                  An Maratho 2018; Chinh phục Cúc Phương; Khám phá Tây Hồ 2018, v.v.</p>
                  </div>
                  <div className="images">
                    <img className="img-achievement" src={require('../../images/cucphuong01.png')} />
                    <img className="img-achievement" src={require('../../images/trangan01.png')} />
                  </div>
                </div>
              </TabPane>
              <TabPane tab="Lĩnh vực đạt được" key={3}>
                <IntroTitle name="Các thành tích đạt được" />
                <p><span className="highlight">Tổ chức các giải chạy cộng đồng quy mô hớn:</span></p>
                <p>Với sự mệnh nâng tầm thể lực cho người Việt, Big Prize phát triển các giải chạy bộ cộng đồng trở thành ngày hội đích thực của tất cả mọi người. Bằng kinh nghiệm
                  và sự sáng tạo của mình, Big Prize luôn thách thức bản thân để mang đến những sự kiện độc đáo hơn, chuyên nghiệp hơn.</p>
                <p><span className="highlight">Tổ chức các chương trình chạy trong nội bộ doanh nghiệp</span><br />
                  Văn hóa doanh nghiệp không chỉ là đặc tính của công ty mà còn là một lợi thế cạnh tranh trong dài hạn. Do đó, xây dựng văn hóa doanh nghiệp có thể coi là một chiến
                  lược kinh doanh dài hạn hiện đại của doanh nghiệp. Chạy bộ với đặc thù dễ tiếp cận, dễ chơi, và tính kết nối, cộng đồng cao là môn thể thao vô cùng phù hợp để xây
                  dựng văn hóa doanh nghiệp. Đặc biệt, chạy bộ hình thành những thói quen tốt dài hạn cho người chơi. Vì thế, việc đầu tư xây dựng văn hóa chạy bộ sẽ là một bước biến
                  chuyển lớn cho doanh nghiệp.</p>
                <p><span className="highlight">Tổ chức các tour du lịch chạy bộ trải nghiệm “Adventure Running Tour”</span><br />
                  Còn gì tuyệt vời hơn khi ta có thể khám phá những nơi “tiên cảnh”, “trong thần thoại”, những nơi xe cộ không thể tiếp cận. Adventure Running Tour là sản phẩm trải
                  nghiệm độc nhất vô nhị của Big Prize. Người chơi sẽ được thỏa mãn sự tò mò, ngây ngất giữa trời mây, cũng như thầm cảm ơn đôi chân đã đưa mình đến được chốt thần
                  tiên. Quan trọng hơn, Big Prize bằng sự chuyên nghiệp và kinh nghiệm của mình sẽ đảm bảo cho bạn có những trải nghiệm an toàn và thú vị nhất.</p>
              </TabPane>
            </Tabs>
          </div>
        </div>
        <Footer />
      </div>
    )
  }
}