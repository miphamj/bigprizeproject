import React, { Component } from 'react';
import { Tabs } from 'antd';

import { Header } from '../../components/header';
import { HeaderCarousel } from '../../components/carousel';
import { TournamentOrganization } from '../../components/tournament-organization';
import { Footer } from '../../components/footer';
import { CategoryTitle } from '../../components/category-title';

import './services.scss';

const { TabPane } = Tabs;
export class Services extends Component {

    constructor(props) {
        super(props);
        this.bgImg = [require('../../images/bg01.jpg'),require('../../images/bg06.jpg'),require('../../images/bg05.jpg')];
    }

    callback(key) {
        
    }

    render() {
        return (
            <div className="services">
                <Header pathName="/services" />
                <HeaderCarousel height="500" bgImg={this.bgImg} />
                <div className="content">
                    <CategoryTitle name="Dịch vụ" />
                    <Tabs className="tabs" defaultActiveKey="1" onChange={this.callback}>
                        <TabPane tab="Tổ chức giải chạy" key="1">
                            <TournamentOrganization {...this.props}  />
                        </TabPane>
                        <TabPane tab="Adventure running tours" key="2">
                            Content of Tab Pane 2
                        </TabPane>
                    </Tabs>
                </div>
                <Footer />
            </div>
        )
    }
}