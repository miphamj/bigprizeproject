import React, { Component } from 'react';

import './tournament-detail.scss'
import { Header } from '../../components/header'
import { BannerHeader } from '../../components/banner-header';
import { TourDetail } from '../../components/tour-detail';
import { Footer } from '../../components/footer';

export class TournamentDetail extends Component {

  constructor(props){
    super(props);
  }
  
  render() {
      const { item } = this.props.history.location.state;
      console.log('item: ', this.props.history.location.state);
    return (
      <div className="container">
        <Header pathName="/services" />
        <BannerHeader imgSource={item} />
        <TourDetail infoDetail={item}/>
        <Footer />
      </div>
    )
  }
}