import React, { Component } from 'react'
import { Router, Switch, Route } from 'react-router-dom'
import { Provider } from 'react-redux';
import { Home } from './guest/home';
import { Services } from './guest/services';
import { Introduce } from './guest/introduce';
import { TournamentDetail } from './guest/tournament-detail';
import { Discovery } from './guest/discovery';
import { DiscoveryDetail } from './guest/discovery-detail';
import Contact from './guest/contact';
import { browserHistory } from '../utils/history'
import PrivateRoute from './private-route'
import { store, persistor } from '../store/custom-store';
import ScrollToTop from '../common/scroll-top'

export default class Root extends Component {

  componentDidMount() {
  }
  render() {
    return (
      <Provider store={persistor}>
        <Router history={browserHistory}>
          <ScrollToTop>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/home" component={Home} />
              <Route path="/services" component={Services} />
              <Route path="/introduce" component={Introduce} />
              <Route path="/tournament-detail" component={TournamentDetail} />
              <Route path="/discovery-detail" component={DiscoveryDetail} />
              <Route path="/news" component={Discovery} />
              <Route path="/contact" component={Contact} />
            </Switch>
          </ScrollToTop>
        </Router>
      </Provider>
    );
  }
}
