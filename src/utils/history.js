const  createHistory = require("history").createBrowserHistory;
const  createMemoryHistory = require("history").createMemoryHistory;
const createHashHistory = require("history").createHashHistory;

export const browserHistory = createHistory();
export const memoryHistory = createMemoryHistory();
export const hashHistory = createHashHistory();